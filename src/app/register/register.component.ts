import { Component, OnInit } from '@angular/core';
import { RestServicesService } from '../shared/services/rest-services.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userObj : any = {}
  constructor(private dataService : RestServicesService) { }

  ngOnInit() {
  }

  registerUser(){
    this.dataService.getdata();
    // this.dataService.post('user/register',this.userObj).subscribe(regResponce =>{
    //   console.log("regResponce",regResponce);
    // });
  }

}
