import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  showpswd : any = false
  constructor() { }

  ngOnInit() {
  }

  showPassword() {
    this.showpswd = !this.showpswd ? true :false;
  }

}
