import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component'
import { PagesComponent } from './pages.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { RestServicesService } from '../shared/services/rest-services.service';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [
    HomeComponent,
    LoginComponent,
    PagesComponent,
    HeaderComponent,
    RegisterComponent
  ],
  providers : [RestServicesService]
})
export class PagesModule { }
