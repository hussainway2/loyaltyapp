import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { HttpClientModule } from  '@angular/common/http';
const routes: Routes = [
  { path: '', component: PagesComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent }

];

@NgModule({
  imports: [HttpClientModule,RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers : []
})
export class PagesRoutingModule { }
