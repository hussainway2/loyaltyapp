'use strict';

const multer = require('multer');
const fs = require('fs');
const fse = require('fs-extra');
const ImagePack = require('imagemagick');
const download = require('image-downloader')
const mv = require('mv');
const XLSX = require('xlsx')
const AppConfig = require("config");
const sharp = require('sharp');
var FileUtils = module.exports;

var prepareStorage = function (upload_path, file_name) {
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, upload_path)
    },
    filename: function (req, file, cb) {
      console.log(req.name);
      cb(null, file_name)
    }
  });

  return storage;
};

FileUtils.upload = function (request, response, upload_root_path, upload_path, file_name, key_name, max_size, is_unlink, type) {
  this.exec = function (callback) {
    if (type == "single") {
      FileUtils.uploadSingle(request, response, upload_root_path, upload_path, file_name, key_name, max_size, is_unlink, callback)
    }
  };
};

FileUtils.uploadSingle = function (request, response, upload_root_path, upload_path, file_name, key_name, max_size, is_unlink, callback) {
  let multer_config = {};
  multer_config.storage = prepareStorage(upload_path, file_name);
  if (max_size) {
    multer_config.limits = {
      fileSize: max_size
    };
  }

  console.log("uploading image");
  let upload = multer(multer_config).single(key_name);
  // console.log("called to convert %%%%%%%%%%%%5");
  // ImagePack.convert([upload_path + "/" + file_name, '-density', 340,  "1_converted.jpg"],
  //   function (err, stdout) {
  //     console.log("$$$$$$$$$$$$$$$$$$$$$$ file name ", upload_path + "/" + file_name, err, stdout);
  //     if (err) {
  //       throw err;
  //     }
  //     console.log('stdout:', stdout);
  //   }
  // );


  if (is_unlink && fs.existsSync(upload_root_path)) {
    fs.unlink(upload_root_path, function (delete_existed_error) {
      if (delete_existed_error) {
        let error = {};
        error.code = "DELETE_EXISTED_ERROR";
        return callback(error, file_name)
      };

      upload(request, response, function (upload_error) {
        return callback(upload_error, file_name);
      });

    });
  } else {
    upload(request, response, function (upload_error) {
      return callback(upload_error, file_name);
    });
  }

};

FileUtils.cropImage = function (source, destination, width, height, quality, gravity, options, callback) {
  gravity = gravity ? gravity : "North";
  quality = quality ? quality : 0.85;
  if (!options) {
    options = {
      srcPath: source,
      dstPath: destination,
      width: width,
      height: height,
      quality: quality,
      gravity: gravity //[NorthWest | North | NorthEast | West | Center | East | SouthWest | South | SouthEast], defaults to Center.
    };
  };

  ImagePack.crop(options, function (error, stdout, stderr) {
    callback(error, stdout, stderr);
  });
};

FileUtils.downloadImage = function (url, file_path, callback) {
  const options = {
    url: url,
    dest: file_path
  };

  download.image(options)
    .then(function (file_name, image) {
      return callback(null, file_name);
    })
    .catch(function (error) {
      return callback(error, null);
    });
};

FileUtils.moveFile = function (source, destination, callback) {
  console.log(source);
  if (fs.existsSync(source)) {
    mv(source, destination, function (error) {
      return callback(error, destination);
    });
  } else {
    return callback("Destination does not exists.", null);
  }

};

FileUtils.readXL = function (file_path, sheet_name, sheet_index, callback) {
  if (!fs.existsSync(file_path)) {

    return callback("Given file path doesn't exists.");
  }

  if (!(sheet_name || sheet_index || sheet_index == 0)) {
    console.log("sheet index error");
    return callback("Neither sheet name nor sheet index given.");
  }


  let workbook = XLSX.readFile(file_path);
  let sheet_name_list = workbook.SheetNames;
  sheet_name = sheet_name ? sheet_name : sheet_name_list[sheet_index];

  var XlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name]);

  return callback(null, XlData);

}

FileUtils.resize = (path, format, width, height) => {
  const readStream = fs.createReadStream(path);
  let transform = sharp();

  if (format) {
    transform = transform.toFormat(format);
  }

  if (width || height) {
    transform = transform.resize(width, height).max();
  }

  return readStream.pipe(transform);
};
