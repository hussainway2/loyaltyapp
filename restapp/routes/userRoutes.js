const userService = require('../services/userService');

module.exports = function(app){
    app.post('/user/register',userService.registerUser);
}