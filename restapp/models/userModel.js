var mongoose = require('mongoose');

Schema = mongoose.Schema

var users = new Schema({
    loyalty_card_number: {
        type: String
    },
    name: {
        type: String
    },
    email: {
        type: String,
        unique: true
    },
    password: {
        type: String
    },
    mobile: {
        type: String
    }
}, {
        versionKey: false
    });

module.exports = mongoose.model('users', users);